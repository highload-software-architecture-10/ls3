const express = require('express');
const app = express();
const {MongoClient} = require('mongodb');
const {generateModel} = require('fake-data-generator');
const {Client} = require('@elastic/elasticsearch');
const cluster = require('node:cluster');

const MongoDBclient = new MongoClient(process.env.ME_CONFIG_MONGODB_URL);
const model = require('./fakeModelConfig.json');
const numCPUs = require('node:os').availableParallelism;
let elasticSearch = null;

if (cluster.isPrimary) {
    console.log(`[CLUSTER] numCPUs: ${numCPUs}`);

    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
        console.log(`[CLUSTER] created.`);

    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`[CLUSTER] ${worker.process.pid} dead.`);
    });
} else {
    (async () => {
        await initDB();
        await initServer();
        await initElasticSearch();
    })();
}


async function initDB() {
    try {
        await MongoDBclient.connect();
        console.log("[DB] connected.");
    } catch (e) {
        console.log(e)
    }
}

async function initServer() {
    app.get('/insert', async (req, res) => {
        const testCollection = await MongoDBclient.db('testdb').collection('test_collection');
        const dataModel = generateData();
        await testCollection.insertOne(dataModel);
        const data = await testCollection.find().toArray();

        const idModel = dataModel._id;
        delete dataModel._id;

        await elasticSearch.index({
            index: 'test_collection',
            id: idModel,
            document: dataModel
        });

        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({status: 'ok', count: data.length, dataModel: dataModel}, null, 2));

        console.log('[SERVER] insert 1 record.')
    });

    app.get('/findAll', async (req, res) => {
        const testCollection = await MongoDBclient.db('testdb').collection('test_collection');
        const data = await testCollection.find().toArray();

        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({status: 'ok', data: data}, null, 2));

        console.log('[SERVER] find all.')
    })


    return new Promise((resolve) => {
        app.listen(8081, () => {
            console.log('[Server] started.');
            resolve();
        });
    })
}

async function initElasticSearch() {
    elasticSearch = new Client({node: process.env.ELASTIC_URL});
    await elasticSearch.cluster.health({});

    console.log('[ElasticSearch] connected.');

    const elasticIndex = await elasticSearch.indices.exists({index: 'test_collection'});

    if (!elasticIndex) {
        await elasticSearch.indices.create({ index: 'test_collection' });
        console.log(`[ElasticSearch] Created index test_collection.`);
    }else {
        console.log(`[ElasticSearch] index test_collection exist.`);
    }
}

function generateData() {
    return generateModel({amountArg: 1, modelArg: model, inputType: 'object', outputType: 'object'});
}
